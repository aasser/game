package p6hiline;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.embed.swing.SwingFXUtils;
import javax.imageio.ImageIO;
/**
 * @author Anneli Asser
 * Nupud on tehtud youtubes oleva tutoriali järgi
 * faili nime genereegimine: http://stackoverflow.com/questions/5242433/create-file-name-using-data-and-time
 */
public class P6hi extends Application {

	Stage window;
	Scene scene1, scene2, scene3;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) {

		
		// Primary stage and scenes and stuff
		window = primaryStage;
		Group root = new Group();

		Canvas canvas = new Canvas(500, 600);
		GraphicsContext gc = canvas.getGraphicsContext2D();

		Canvas canvas1 = new Canvas(500, 600);
		GraphicsContext gc1 = canvas1.getGraphicsContext2D();
		root.setStyle("-fx-background-color: red");

		// Button 1
		Label label1 = new Label("Tere tulemust mängu nimega 'Peeter'");
		
		Button button1 = new Button("Alusta");
		button1.setOnAction(e -> window.setScene(scene3));

		VBox layout1 = new VBox(20);
		layout1.getChildren().addAll(label1, button1);
		
		scene1 = new Scene(layout1, 200, 200);
		
		scene1.getStylesheets().add("stiil.css");

		// Button 2
		Button button2 = new Button("Valmis");
		button2.setOnAction(e -> {
			boolean result = GameOver.display("Mäng 'Peeter'", "Mängi uuesti?");

			if (result == true) {
				window.setScene(scene1);
				gc1.clearRect(0, 0, 500, 600);
			} else {

				WritableImage wim = new WritableImage(500, 600);
				root.snapshot(null, wim);

				Date date = new Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
				File file = new File(dateFormat.format(date) + ".png");
				try {
					ImageIO.write(SwingFXUtils.fromFXImage(wim, null), "png", file);
				} catch (Exception s) {
				}
				System.out.println("Mäng saigi läbi");
				System.exit(0);

			}

		});

		// Tegelane paljas Peeter
		Joonistaja tegelane1 = new Joonistaja();
		tegelane1.joonistaBody(gc);
		Joonistaja tegelane2 = new Joonistaja();
		tegelane2.joonistaPea(gc);

		// Klouni kostüüm
		Joonistaja kloun = new Joonistaja();
		Button button3 = new Button("Kloun");
		button3.setOnAction(e -> {
			gc1.clearRect(0, 0, 500, 600);
			kloun.joonistaKloun(gc1);
			tegelane2.joonistaPea(gc1);

		});

		// Santa kostüüm
		Joonistaja santa = new Joonistaja();
		Button button4 = new Button("Santa");
		button4.setOnAction(e -> {
			gc1.clearRect(0, 0, 500, 600);
			tegelane2.joonistaPea(gc1);
			santa.joonistaSanta(gc1);

		});

		// Vertikaalselt nupude asetus
		VBox vbox = new VBox(10); // spacing = 10
		vbox.getChildren().addAll(button3, button4, button2);

		// panen gruppi root - canvas ja canvas1
		root.getChildren().addAll(canvas, canvas1);
		
		

		// Teen uue SplitPane ja panen sinna sisse Vbox ja root grupi
		SplitPane slitp = new SplitPane();
		slitp.getItems().addAll(vbox, root);
		// Teen uue stseeni - scene3
		scene3 = new Scene(slitp);
		// panen kogu akna sisuks scene 3
		window.setScene(scene3);

		// Display scene 1 at first
		window.setScene(scene1);
		window.setTitle("Mäng 'Peeter'");
		window.show();

	}

}
