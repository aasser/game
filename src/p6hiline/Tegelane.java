package p6hiline;

import javafx.application.Application;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Tegelane {

	private void joonistaKael(GraphicsContext gc) {
		gc.setFill(Color.WHEAT);
		gc.fillRoundRect(175, 240, 50, 40, 20, 20);
	}

	private void joonistaKeha(GraphicsContext gc) {
		// Keha
		gc.setFill(Color.WHEAT);
		gc.fillRoundRect(75, 260, 250, 250, 20, 20);
	}

	private void joonistaNagu(GraphicsContext gc) {
		// näo põhi
		gc.setFill(Color.WHEAT);
		gc.fillOval(100, 50, 200, 200);

	}

	private void joonistaSilmad(GraphicsContext gc) {

		// silma valged
		gc.setFill(Color.WHITE);
		gc.fillOval(158, 121, 35, 10);
		gc.fillOval(208, 121, 35, 10);

		// Silma puhkpillid
		gc.setFill(Color.BLACK);
		gc.fillOval(170, 120, 10, 10);
		gc.fillOval(220, 120, 10, 10);

		// silma kontuur sinine
		gc.setStroke(Color.BLUE);
		gc.setLineWidth(1);
		// silma puhkpilli stroke
		gc.strokeOval(158, 121, 35, 10);
		gc.strokeOval(208, 121, 35, 10);

	}

	private void joonistaHuuled(GraphicsContext gc) {
		// Huuled
		gc.setFill(Color.RED);
		gc.fillRoundRect(155, 183, 90, 40, 100, 100);
	}

	private void joonistaSuuSisemus(GraphicsContext gc) {

		// suu sisemus
		gc.setFill(Color.DARKRED);
		gc.fillRoundRect(170, 190, 60, 25, 100, 100);
		// hambad
		gc.setFill(Color.WHITE);
		// ülemised
		gc.fillRoundRect(185, 190, 30, 5, 50, 50);
		// alumised
		gc.fillRoundRect(185, 210, 30, 5, 50, 50);
	}

	private void joonistaNina(GraphicsContext gc) {

		gc.setStroke(Color.BLACK);
		gc.setLineWidth(1);
		// gc.strokeLine(x1, y1, x2, y2);
		gc.strokeLine(195, 160, 200, 150);
		gc.strokeLine(195, 160, 205, 160);
	}

	public void joonistaBody(GraphicsContext gc) {

		Tegelane tegelane = new Tegelane();
		tegelane.joonistaNagu(gc);
		tegelane.joonistaKeha(gc);
		tegelane.joonistaKael(gc);

	}

	public void joonistaPea(GraphicsContext gc) {

		Tegelane tegelane = new Tegelane();

		tegelane.joonistaSilmad(gc);
		tegelane.joonistaHuuled(gc);
		tegelane.joonistaSuuSisemus(gc);
	}

}
