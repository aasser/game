package p6hiline;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import kostyymid.*;

public class Joonistaja {

	public void joonistaPea(GraphicsContext gc) {

		Tegelane pea = new Tegelane();
		pea.joonistaPea(gc);
	}

	public void joonistaBody(GraphicsContext gc) {

		Tegelane keha = new Tegelane();
		keha.joonistaBody(gc);

	}

	public void joonistaKloun(GraphicsContext gc) {

		Kloun kloun = new Kloun();
		kloun.joonista(gc);

	}

	public void joonistaSanta(GraphicsContext gc) {

		Santa santa = new Santa();
		santa.joonista(gc);

	}

}
