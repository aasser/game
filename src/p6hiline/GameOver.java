package p6hiline;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;

/**
 * @author Anneli Asser
 * Nupud on tehtud youtubes oleva tutoriali järgi
 * 
 */
public class GameOver {

	static boolean vastus;

	public static boolean display(String title, String message) {
		Stage window = new Stage();

		// Block events to other windows
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle(title);
		window.setWidth(300);
		window.setHeight(200);
		Label label = new Label();
		label.setText(message);

		// Create two buttons
		Button yesButton = new Button("Jah!");
		Button noButton = new Button("Salvesta ja Sulge");
		yesButton.setMaxSize(200, 20);
		noButton.setMaxSize(200, 20);

		// Clicking will set answer and close window
		yesButton.setOnAction(e -> {
			vastus = true;
			window.close();
		});
		noButton.setOnAction(e -> {
			vastus = false;
			window.close();
		});

		GridPane grid = new GridPane();
		// terve grid
		grid.setPadding(new Insets(10, 10, 10, 10));
		// cells
		grid.setVgap(10);
		grid.setHgap(10);

		GridPane.setConstraints(label, 1, 1);
		GridPane.setConstraints(yesButton, 1, 2);
		GridPane.setConstraints(noButton, 2, 2);

		grid.getChildren().addAll(label, yesButton, noButton);

		// Display window and wait for it to be closed before returning
		Scene scene = new Scene(grid);
		window.setScene(scene);
		window.showAndWait();

		return vastus;
	}

}
