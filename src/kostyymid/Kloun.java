package kostyymid;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Kloun {
	public void joonista(GraphicsContext gc) {

		Kloun kloun = new Kloun();

		kloun.joonistaKeha(gc);
		kloun.joonistaLips(gc);
		kloun.joonistaJuuksedV(gc);
		kloun.joonistaJuuksedP(gc);
		kloun.joonistaTopHat(gc);
		kloun.joonistaBlush(gc);
		kloun.joonistaEyeShadow(gc);
		kloun.joonistaSuuTaust(gc);
		kloun.joonistaNina(gc);
	}

	private void joonistaKeha(GraphicsContext gc) {
		// Keha
		gc.setFill(Color.PURPLE);
		gc.fillRoundRect(75, 260, 250, 250, 20, 20);
	}

	private void joonistaLips(GraphicsContext gc) {
		// lips vasak pool
		gc.setFill(Color.RED);
		double xpoints[] = { 242, 242, 200 };
		double ypoints[] = { 245, 290, 270 };
		int num = 3;
		gc.fillPolygon(xpoints, ypoints, num);
		// lips parem pool
		gc.setFill(Color.RED);
		double xpoints1[] = { 160, 160, 200 };
		double ypoints1[] = { 245, 290, 270 };
		int num1 = 3;
		gc.fillPolygon(xpoints1, ypoints1, num1);
		// lipsu kesk
		gc.setFill(Color.RED);
		gc.fillOval(190, 255, 20, 30);

	}

	private void joonistaJuuksedV(GraphicsContext gc) {

		// juuksed vasak pool
		gc.setFill(Color.ORANGE);
		gc.fillOval(100, 55, 50, 50);
		gc.fillOval(90, 80, 30, 30);
		gc.fillOval(115, 85, 35, 30);
		gc.setStroke(Color.WHITE);
		gc.strokeOval(100, 55, 50, 50);
	}

	private void joonistaJuuksedP(GraphicsContext gc) {
		// Juuksed parem pool
		gc.setFill(Color.ORANGE);
		gc.fillOval(250, 55, 50, 50);
		gc.fillOval(275, 80, 30, 30);
		gc.fillOval(250, 85, 35, 30);
		gc.setStroke(Color.WHITE);
		gc.strokeOval(250, 55, 50, 50);

	}

	private void joonistaTopHat(GraphicsContext gc) {
		// Müts tophat
		gc.setFill(Color.BLACK);
		gc.fillRoundRect(100, 50, 200, 35, 60, 60);
		gc.fillRoundRect(120, 0, 160, 70, 60, 60);
		gc.setFill(Color.WHITE);
		gc.fillRoundRect(120, 49, 160, 2, 1, 1);

	}

	private void joonistaBlush(GraphicsContext gc) {
		// Blush
		gc.setFill(Color.RED);
		gc.fillOval(110, 139, 35, 35);
		gc.fillOval(255, 139, 35, 35);

	}

	private void joonistaEyeShadow(GraphicsContext gc) {

		// silma sisemused valged
		gc.setFill(Color.WHITE);
		gc.fillOval(150, 90, 50, 70);
		gc.fillOval(200, 90, 50, 70);

	}

	private void joonistaNina(GraphicsContext gc) {

		// See on nina
		gc.setFill(Color.RED);
		gc.fillOval(182, 139, 35, 35);
	}

	private void joonistaSuuTaust(GraphicsContext gc) {
		// See on suu valge
		gc.setStroke(Color.WHITE);
		gc.setLineWidth(30);
		gc.strokeRoundRect(155, 183, 90, 40, 60, 60);
	}
}
